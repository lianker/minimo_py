import pytest

# Fixture Example
@pytest.fixture(scope="function")
def fixture_hello():
    """
    Fixture Example
    """
    return {"name": "Hello Fixture"}
